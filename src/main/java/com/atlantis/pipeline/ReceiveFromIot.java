package com.atlantis.pipeline;


import com.rabbitmq.client.*;


import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class ReceiveFromIot {

    private Connection connection;

    public ReceiveFromIot() throws IOException, TimeoutException {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("broker.atlantis.com");
        connection = factory.newConnection();
    }

    public void channel_connexion(String channel_name, DeliverCallback deliverCallback) throws IOException {
        Channel channel = connection.createChannel();
        channel.exchangeDeclare(channel_name, BuiltinExchangeType.DIRECT);
        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, channel_name, "");
        channel.basicConsume(queueName, true, deliverCallback, consumerTag -> { });
    }

}
