package com.atlantis.pipeline;

import java.util.UUID;

public class JsonMetric {
    private UUID deviceId;
    private String deviceType;
    private String metricDate;
    private String metricValue;

    @Override
    public String toString() {
        return "JsonMetric{" +
                "deviceId=" + deviceId +
                ", deviceType='" + deviceType + '\'' +
                ", metricDate='" + metricDate + '\'' +
                ", metricValue='" + metricValue + '\'' +
                '}';
    }

    public UUID getDeviceId() {
        return deviceId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public String getMetricDate() {
        return metricDate;
    }

    public String getMetricValue() {
        return metricValue;
    }
}
