package com.atlantis.pipeline;

import java.util.UUID;

public class JsonDevice {
    private UUID id;
    private String name;
    private String deviceType;

    public UUID getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDeviceType() {
        return deviceType;
    }

    @Override
    public String toString() {
        return "JsonDevice{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", deviceType='" + deviceType + '\'' +
                '}';
    }
}
