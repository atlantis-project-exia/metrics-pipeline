package com.atlantis.pipeline;


import com.atlantis.entities.GenericValue;
import com.atlantis.entities.models.*;
import com.atlantis.entities.services.*;
import com.google.gson.Gson;
import com.rabbitmq.client.DeliverCallback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeoutException;

@SpringBootApplication(scanBasePackages = {
        "com.atlantis.entities.repositories",
        "com.atlantis.entities.services"
})
@EntityScan("com.atlantis.entities.models")
@EnableJpaRepositories("com.atlantis.entities.repositories")
public class SpringTemplateApplication {

    private ReceiveFromIot recv = new ReceiveFromIot();

    private Gson gson = new Gson();

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private DeviceTypeService deviceTypeService;

    @Autowired
    private MetricService metricService;

    @Autowired
    private MetricTypeService metricTypeService;

    public SpringTemplateApplication() throws IOException, TimeoutException {
    }

    public static void main(String[] args) {
        SpringApplication.run(SpringTemplateApplication.class, args);
    }

    @Bean
    public CommandLineRunner receive_devices() {
        return (args) -> {
            String ch = "devices";
            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String json = new String(delivery.getBody(), StandardCharsets.UTF_8);
                JsonDevice jsonDevice = gson.fromJson(json, JsonDevice.class);
                System.out.printf("[%s] Received '%s'%n", ch, jsonDevice);
                Device device = new Device();
                device.setName(jsonDevice.getName());
                device.setId(jsonDevice.getId());
                device.setDevice_type(
                    deviceTypeService.findOneByName(jsonDevice.getDeviceType())
                );
                deviceService.save(device);
            };

            recv.channel_connexion(ch, deliverCallback);
        };
    }

    @Bean
    public CommandLineRunner receive_metrics() throws ParseException {
        return (args) -> {
            String ch = "metrics";
            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String json = new String(delivery.getBody(), StandardCharsets.UTF_8);
                JsonMetric jsonMetric = gson.fromJson(json, JsonMetric.class);
                System.out.printf("[%s] Received '%s'%n", ch, jsonMetric);
                Metric metric = new Metric();
                Device device = deviceService.getOne(jsonMetric.getDeviceId());
                metric.setDevice(device);
                metric.setCreated_at(
                        LocalDateTime.parse(
                                jsonMetric.getMetricDate(),
                                DateTimeFormatter.ISO_LOCAL_DATE_TIME
                        )
                );

                MetricType metricType = metricTypeService.findOneByName(jsonMetric.getDeviceType());
                metric.setMetric_type(metricType);

                switch (metricType.getType()) {
                    case "int":
                        GenericValue<Integer> gv1 = new GenericValue<>();
                        gv1.setValue(Integer.valueOf(jsonMetric.getMetricValue()));
                        metric.setVal(gv1);
                        break;
                    case "float":
                        GenericValue<Float> gv2 = new GenericValue<>();
                        gv2.setValue(Float.valueOf(jsonMetric.getMetricValue()));
                        metric.setVal(gv2);
                        break;
                    case "bool":
                        GenericValue<Boolean> gv3 = new GenericValue<>();
                        gv3.setValue(Boolean.valueOf(jsonMetric.getMetricValue()));
                        metric.setVal(gv3);
                        break;
                    case "string":
                        GenericValue<String> gv = new GenericValue<>();
                        gv.setValue(jsonMetric.getMetricValue());
                        metric.setVal(gv);
                        break;
                }

                metricService.save(metric);
            };

            recv.channel_connexion(ch, deliverCallback);
        };
    }
}
