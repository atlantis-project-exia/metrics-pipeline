# Getting Started

Change `application.properties` in the directory `resources` to math your database connection. 

To build : `./mvnw install`

To run : `java -jar target/spring-template-0.0.1-SNAPSHOT.jar`

### Reference Documentation
For further reference, please consider the following sections:

* [Spring REST docs](https://spring.io/guides/gs/rest-service/)

